const register = document.querySelector('#registerUser');

register.addEventListener('submit', (e) => {
    e.preventDefault()

    let firstName = document.querySelector('#firstName').value
    let lastName = document.querySelector('#lastName').value
    let email = document.querySelector('#email').value
    let mobileNo = document.querySelector('#mobileNumber').value
    let password1 = document.querySelector('#password1').value  
    let password2 = document.querySelector('#password2').value    



     

     //lets create a validation to enable the submit button when all fields are populated and both passwords should match otherwise it should not continue.
     if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {

        //if all the requirements are met, then now lets check for duplicate emails in the database first. 
        fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/email-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data)
           
            if(data === false){
                fetch('https://peaceful-plateau-05404.herokuapp.com/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email : email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    
                    if(data === true){
                        Swal.fire({
                            icon: 'success',
                            title: 'Success...',
                            text: 'Registration Successful!',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                              },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                           
                          })
                        
                        
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                              },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                           
                          })
                    }
                })
            }else {
                Swal.fire({
                    icon: 'error',
                    title: 'Email address already exists',
                    text: 'Please use another email address!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                      },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                        }
                   
                  })
            }
        })
    }


    
})
