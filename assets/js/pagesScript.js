

const btn = document.getElementById('sidebarCollapse');
const sidebar = document.getElementById('sidebar');


    btn.addEventListener('click', function(){ 
            sidebar.classList.toggle('active');
            btn.classList.toggle('active');

    });



let pointer = document.querySelector('.cursor');




window.addEventListener('mousemove', cursor);


function cursor(e){
    pointer.style.top = e.pageY + 'px';
    pointer.style.left = e.pageX + 'px';
}



sidebar.addEventListener('mouseover', () => {
        pointer.classList.add('white');
    });
sidebar.addEventListener('mouseleave', () => {
        pointer.classList.remove('white');
        pointer.classList.add('cursor');
    });




const adminBtn = document.querySelector('#buttons')


const navItems = document.querySelector('#navSession');

let userToken = localStorage.getItem('token')



if(!userToken) {

    adminBtn.classList.add('d-none');

    navItems.innerHTML = `

    <li class="nav-item">
        <a href="../pages/login.html" class="nav-link" id="loginLink"> Log in </a>
    </li> `

    
   
}else {
    fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details',
                
    {
        headers: {
            Authorization: `Bearer ${userToken}`
        }
    })
    .then(res => res.json())
    .then(data => {
        if(data.isAdmin == false){
            navItems.innerHTML = `
            <li class="nav-item">
                <a href="../pages/profile.html" id="profileLink">Profile</a>
             </li>
            <li class="nav-item">
                <a href="../pages/logout.html" id="logoutLink" class="nav-link"> Log Out </a>
            </li>`
           
        }else {
            navItems.innerHTML = `
            <li class="nav-item">
                <a href="./addCourse.html" id="addLink" class="nav-link"> Add Course</a>
            </li> 
            <li class="nav-item">
                <a href="../pages/logout.html" id="logoutLink" class="nav-link"> Log Out </a>
            </li>
            <li class="nav-item">
                <a href="../pages/archives.html" id="archiveLink" class="nav-link"> Archives </a>
            </li>`

        }

    })

   
}