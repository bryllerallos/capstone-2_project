
const courseSearch = document.querySelector('#courseSearch');

let access = localStorage.getItem('token')


const queryId = window.location.search;
const urlParams = new URLSearchParams(queryId);
const id = urlParams.get('id')



const courseId= document.querySelector('#id');
const name= document.querySelector('#name');
const desc= document.querySelector('#desc');
const price= document.querySelector('#price');

fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/${id}`, {
    headers:{
        Authorization: `Bearer ${access}`
    }
        })
        .then(res => res.json())
        .then(course => {

        
            courseId.innerText = `Course id: ${course._id}` 
            name.innerText = `Course Name: ${course.name}`
            desc.innerText = `Description: ${course.description}`
            price.innerText = `Price: ₱ ${course.price}`

           

fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details', {

    headers: {
        Authorization: `Bearer ${access}`
    }

})
    .then(res => res.json())
    .then(user => {
        
        if(user.isAdmin === false){
            const client = document.querySelector('#client');
            const admin = document.querySelector('#admin');
            admin.classList.add('d-none')
            client.classList.remove('d-none')

            const enrollments = user.enrollments

            for(let i = 0; i <enrollments.length;i++){

                fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/${enrollments[i].courseId}`,{

								headers: {
									Authorization: `Bearer ${access}`
								}

							})
								.then(res => res.json())
								.then(details => {

                                    console.log(details)
									let courseName = document.createElement('li')
									courseName.classList.add('listStyle')
									courseName.innerText = details.name
									document.querySelector('#courseName').appendChild(courseName)

								})
       

                            let status = document.createElement('li')
                            status.classList.add('listStyle')
                            status.innerText = enrollments[i].status
                            document.querySelector('#status').appendChild(status)

                                
                            }
                        }else {

                            admin.classList.remove('d-none')
                            client.classList.add('d-none')

            const enrollees = course.enrollees;
            console.log(enrollees)

            for(let i = 0; i<enrollees.length;i++){

                fetch(`https://peaceful-plateau-05404.herokuapp.com/api/users/${enrollees[i].userId}`, {

                    headers: {
                            Authorization: `Bearer ${access}`
                        }
    
                    })

                    .then(res => res.json())
                    .then(details => {
                        
                        let enrolled = document.createElement('li')
                        enrolled.innerText = `${details.firstName} ${details.lastName}`
                        enrolled.classList.add('listStyle')
                        document.querySelector('#enrolled').appendChild(enrolled)

                    })
                    


                        let active = document.createElement('li')
                        active.classList.add('listStyle')

                    if(course.isActive == true){
                    active.innerText = 'Active'
                    document.querySelector('#active').appendChild(active)
                     }else {
                     active.innerText = 'Inactive'
                     document.querySelector('#active').appendChild(active)
                     }   



            }

        }
        
    })

})


