
const courseSearch = document.querySelector('#courseSearch');

let access = localStorage.getItem('token')


const queryId = window.location.search;
const urlParams = new URLSearchParams(queryId);
const id = urlParams.get('id')



const courseId= document.querySelector('#id');
const name= document.querySelector('#name');
const desc= document.querySelector('#desc');
const price= document.querySelector('#price');

fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/${id}`, {
    headers:{
        Authorization: `Bearer ${access}`
    }
        })
        .then(res => res.json())
        .then(course => {

        
            courseId.innerText = `Course id: ${course._id}` 
            name.innerText = `Course Name: ${course.name}`
            desc.innerText = `Description: ${course.description}`
            price.innerText = `Price: ₱ ${course.price}`

           


        
    })


courseSearch.addEventListener('submit', (e) => {
    e.preventDefault()

    const courseId = document.querySelector('#course').value

    console.log(courseId)
    
    if(courseId !== ''){
        fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/activate/${id}`, {
                method: 'PATCH',
                headers:{
                    Authorization: `Bearer ${access}`,
                    'Content-Type': 'application/json',
                },
                
                body: JSON.stringify({
                    
                    courseId: courseId,
                    isActive: true
                    
                 })
                 
        })
        .then(res => res.json())
        .then(data => {
           Swal.fire({
                icon: 'success',
                title: 'Success',
                text: 'Course Reactivation Successful!',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                    }
            
            })
        })

    }else {
        Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Course Reactivation Fail!',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                    }
            
            })

    }
})

