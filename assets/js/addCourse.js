const addCourse = document.querySelector('#createCourse');

const token = localStorage.getItem('token');




addCourse.addEventListener('submit', (e) => {
    e.preventDefault()

    let name = document.querySelector('#courseName').value
    let description = document.querySelector('#courseDescription').value
    let price = document.querySelector('#coursePrice').value

    if(!token){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
                }
           
          })
    }else {
        fetch('https://peaceful-plateau-05404.herokuapp.com/api/courses/add', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
    
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => {
            return res.json()
    
        })
        .then(data => {
    
            console.log(data)
           if(data === true) {
            Swal.fire({
                icon: 'success',
                title: 'Course',
                text: 'Added Successfully!',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                    }
               
              })
    
           }else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                  },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                    }
               
              })
           }
    
    
        })
    }




})