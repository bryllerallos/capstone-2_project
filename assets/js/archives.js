

const buttons = document.querySelector('#buttons')

const withToken = document.querySelector('#token')

let access = localStorage.getItem('token')



if(access){
	fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details',
                
                {
                    headers: {
                        Authorization: `Bearer ${access}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                
					

					if(data.isAdmin == true){

						const get = fetch('https://peaceful-plateau-05404.herokuapp.com/api/courses/archives',

						{
							headers: {
								Authorization: `Bearer ${access}`
							}
						})

							get.then(res => res.json())
							.then(data => {
								

								let course;

								for(let i = 0; i <= data.length;i++){
									if(course){
										course = `${course}
										<div class="col-lg-4">
											<div class="card m-3 courses-card">
												
												<h3 class="card-header custom-card-header">${data[i].name}</h3>
									
												<div class="card-body">	
													<p class="card-text">${data[i].description}</p>
													<p class="card-text">&#x20B1 ${data[i].price}</p>
													<h4 class="card-header"><a href="course.html?id=${data[i]._id}"class="card-header btn custom-button badge-pill mx-3">View Details</a></h4>
												</div>
												<div class="card-footer">
													<a href="activate.html?id=${data[i]._id}" class="btn custom-button badge-pill mx-2"><i class="fas fa-power-off fa-lg"></i></a>
												</div>
											</div>
										  </div>`;
									}else {
										course =
										`<div class="col-lg-4">
											<div class="card m-3 courses-card">

												<h3 class="card-header custom-card-header">${data[i].name}</h3>
												
												<div class="card-body">	
													<p class="card-text">${data[i].description}</p>
													<p class="card-text">&#x20B1 ${data[i].price}</p>
													<h4 class="card-header"><a href="course.html?id=${data[i]._id}"class="card-header btn custom-button badge-pill mx-3">View Details</a></h4>
												</div>
												<div class="card-footer">
                                                    <a href="activate.html?id=${data[i]._id}" class="btn custom-button badge-pill mx-2"><i class="fas fa-power-off fa-lg"></i></a>
												</div>
											</div>
									 	 </div>`;
									}

									
									if(course){
										document.querySelector('#courseDetails').innerHTML = course;
									}

								}
									
								
							})
							
					}else {

						

					}
				})

			
}else {
	const fetch = document.querySelector('#fetch');
	fetch.classList.remove('d-none')
}





