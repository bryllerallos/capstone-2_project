let loginForm = document.querySelector("#loginUser")


loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let email = document.querySelector("#userEmail").value
    let password  = document.querySelector("#password").value   
   

    //lets now create a control structure that will allow us to determine if there is data inserted in the given fields
    if(email == "" || password == "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please Enter your Username and Password!',
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
                }
           
          })
    } 
    else {
        fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
               email: email,
               password: password 
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
           
            
            if(data.access){
              
                localStorage.setItem('token', data.access); 
               
                fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details',
                
                {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)

                    if(data.isAdmin == true){

                        window.location.replace('./courses.html')
                    }else {
                        window.location.replace("./profile.html")
                    }


                })
            } else {
              
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Invalid Username or Password',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                      },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                        }
                   
                  })
            }
        })
    }

})