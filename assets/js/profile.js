
const name = document.querySelector('#name');
const email = document.querySelector('#email');
const mobile = document.querySelector('#mobile');

const div = document.querySelector('#div')
const withToken = document.querySelector('#token')



let access = localStorage.getItem('token')


			if(!access) {
				div.classList.remove('d-none')
				withToken.classList.add('d-none')

			}else {
				div.classList.add('d-none')
				withToken.classList.remove('d-none')
				fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details',
                
                {
                    headers: {
                        Authorization: `Bearer ${access}`
                    }
                })
                .then(res => res.json())
                .then(data => {
				

					if(data.isAdmin == true){
						div.classList.remove('d-none')
						withToken.classList.add('d-none')
					}else {
						name.innerHTML = `Name: ${data.firstName.toUpperCase()} ${data.lastName.toUpperCase()}`
                		email.innerHTML = `Email: ${data.email}`
						mobile.innerHTML = `Mobile: ${data.mobileNo}`
						

						const enrollments = data.enrollments;



						const enroll = enrollments.filter(function(enrollment) {
							if(enrollment.status === 'Enrolled') {
								return true
							}
						})

						fetch('https://peaceful-plateau-05404.herokuapp.com/api/courses',
					
							{
								headers: {
									Authorization: `Bearer ${access}`
								}
							})
							.then(res => res.json())
							.then(data => {
								
							
							

						for(let i = 0; i < enroll.length; i++){


							fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/${enroll[i].courseId}`,{

								headers: {
									Authorization: `Bearer ${access}`
								}


							})
								.then(res => res.json())
								.then(details => {

									let id = document.createElement('li')
									id.classList.add('listStyle')
									id.innerText = details.name
									document.querySelector('#courseId').appendChild(id)

								})

									let date = document.createElement('li')
									date.classList.add('listStyle')
									date.innerText = enroll[i].enrolledOn
									document.querySelector('#date').appendChild(date)

									let status = document.createElement('li')
									status.classList.add('listStyle')
									status.innerText = enroll[i].status
									document.querySelector('#status').appendChild(status)

							
						}
					})

							
							
					}
                	

                })
               
			}


               
                