const editCourse = document.querySelector('#editCourse');



const editName = document.querySelector('#courseName')
const editDesc = document.querySelector('#courseDescription')
const editPrice = document.querySelector('#coursePrice')



let access = localStorage.getItem('token')


const queryId = window.location.search;
const urlParams = new URLSearchParams(queryId);
const id = urlParams.get('id')



const courseId = document.querySelector('#courseId')

fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details',
                
    {
        headers: {
            Authorization: `Bearer ${userToken}`
        }
    })
    .then(res => res.json())
    .then(data => {
        
        if(data.isAdmin === true){

            fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/${id}`, {
                method: `GET`,
                headers: {
                    Authorization: `Bearer ${access}`
                }
            })
            .then(res => res.json())
            .then(edit => {
                editName.value = edit.name
                editDesc.value = edit.description
                editPrice.value = edit.price
            }) 





            editCourse.addEventListener('submit', (e) => {
                e.preventDefault()

                const name = document.querySelector('#courseName').value
                const description = document.querySelector('#courseDescription').value
                const price = document.querySelector('#coursePrice').value

            


                fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/edit/${id}`, {
            
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${access}`
                
                     },
                    body: JSON.stringify({
    
                        name: name,
                        description: description,
                        price: price
                    })
               })
                .then(res => res.json())
                .then(data => {
               
                    if((name === '') || (description === '') && (price === '')){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Please input the details!',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                          })
                    }else {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'Updated Successfully',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                          })

                    } 

            })

   
})

        }else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please login as an Admin!',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                    }
              })

        
        }

    })













