const deletedCourse = document.querySelector('#deleteCourse');

let access = localStorage.getItem('token')


const queryId = window.location.search;
const urlParams = new URLSearchParams(queryId);
const id = urlParams.get('id')

const courseId = document.querySelector('#courseId')


fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/${id}`, {
    headers:{
        Authorization: `Bearer ${access}`,
    }
        })
        .then(res => res.json())
        .then(data => {

            courseId.innerText = `Course id: ${data._id}` 
        
})

        if(!access){

            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                    }
            
            })
        }else {
            
            deletedCourse.addEventListener('submit', (e) => {
                e.preventDefault()

                let toDelete = document.querySelector('#courseDelete').value;

                if(toDelete == null || toDelete == ''){

                    Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                        }
                  })
            
                 

                }else {


                            fetch(`https://peaceful-plateau-05404.herokuapp.com/api/courses/delete/${id}`, {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json',
                                    Authorization: `Bearer ${access}`
                                },
                                body: JSON.stringify({
                                    courseId: courseId
                                    
                                })
                            })
                            .then(res => res.json())
                            .then(data => {

                                console.log(data)
                                if(data == true){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Success',
                                        text:  'Deleted Successfully!',
                                        showClass: {
                                            popup: 'animate__animated animate__fadeInDown'
                                        },
                                        hideClass: {
                                            popup: 'animate__animated animate__fadeOutUp'
                                            }
                                    
                                    })
                                }else{
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong!',
                                        showClass: {
                                            popup: 'animate__animated animate__fadeInDown'
                                        },
                                        hideClass: {
                                            popup: 'animate__animated animate__fadeOutUp'
                                            }
                                    
                                    })
                             }
                  })
                }
             })
        }

