

const buttons = document.querySelector('#buttons')

const withToken = document.querySelector('#token')

let access = localStorage.getItem('token')



if(access){
	fetch('https://peaceful-plateau-05404.herokuapp.com/api/users/details',
                
                {
                    headers: {
                        Authorization: `Bearer ${access}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                
					

					if(data.isAdmin == true){

						const get = fetch('https://peaceful-plateau-05404.herokuapp.com/api/courses/',

						{
							headers: {
								Authorization: `Bearer ${access}`
							}
						})

							get.then(res => res.json())
							.then(data => {
								

								let course;

								for(let i = 0; i <= data.length;i++){
									if(course){
										course = `${course}
										<div class="col-lg-4">
											<div class="card m-3 courses-card">
												
												<h3 class="card-header custom-card-header">${data[i].name}</h3>
									
												<div class="card-body">	
													<p class="card-text">${data[i].description}</p>
													<p class="card-text">&#x20B1 ${data[i].price}</p>
													<h4 class="card-header"><a href="course.html?id=${data[i]._id}"class="card-header btn custom-button badge-pill mx-3">View Details</a></h4>
												</div>
												<div class="card-footer">
													<a href="archive.html?id=${data[i]._id}" class="btn custom-button badge-pill mx-2"><i class="far fa-times-circle fa-lg"></i></a>

													<a href="deleteCourse.html?id=${data[i]._id}" class="btn custom-button badge-pill mx-3"><i class="fas fa-trash-alt" id="delete"></i></a>
													<a href="editCourse.html?id=${data[i]._id}" class="btn custom-button badge-pill"><i class="fas fa-pencil-alt" id="edit"></i></a>								
												</div>
											</div>
										  </div>`;
									}else {
										course =
										`<div class="col-lg-4">
											<div class="card m-3 courses-card">

												<h3 class="card-header custom-card-header">${data[i].name}</h3>
												
												<div class="card-body">	
													<p class="card-text">${data[i].description}</p>
													<p class="card-text">&#x20B1 ${data[i].price}</p>
													<h4 class="card-header"><a href="course.html?id=${data[i]._id}"class="card-header btn custom-button badge-pill mx-3">View Details</a></h4>
												</div>
												<div class="card-footer">
													<a href="archive.html?id=${data[i]._id}" class="btn custom-button badge-pill mx-2"><i class="far fa-times-circle fa-lg"></i></a>

													<a href="deleteCourse.html?id=${data[i]._id}" class="btn custom-button badge-pill mx-3"><i class="fas fa-trash-alt"  id="delete"></i></a>
													<a href="editCourse.html?id=${data[i]._id}" class="btn custom-button badge-pill"><i class="fas fa-pencil-alt" id="edit"></i></a>
													
												</div>
											</div>
									 	 </div>`;
									}

									
									if(course){
										document.querySelector('#courseDetails').innerHTML = course;
									}

								}
									
								
							})
							
					}else {

						buttons.classList.add('d-none')

						const get = fetch('https://peaceful-plateau-05404.herokuapp.com/api/courses/')
							get.then(res => res.json())
							.then(data => {

								
								let course;
								for(let i = 0; i <= data.length;i++){
									if(course){
										course = `${course}
										<div class="col-lg-4">
											<div class="card m-3">
												<h3 class="card-header custom-card-header bg-dark text-light">${data[i].name}</h3>
												<div class="card-body">	
													<p class="card-text">${data[i].description}</p>
													<p class="card-text">&#x20B1 ${data[i].price}</p>
													<h4 class="card-header"><a href="course.html?id=${data[i]._id}"class="card-header btn custom-button badge-pill mx-3">View Details</a></h4>
												</div>
												<div class="card-footer bg-dark text-light">

													<a href="enroll.html?id=${data[i]._id}" class="btn text-light custom-button badge-pill"><i class="fas fa-plus mx-1" id="enroll"></i>Sign Up</a>
												</div>
											</div>
										  </div>`;
									}else {
										course =
										`<div class="col-lg-4">
											<div class="card m-3">
												<h3 class="card-header custom-card-header bg-dark text-light">${data[i].name}</h3>
												<div class="card-body">	
													<p class="card-text"> ${data[i].description}</p>
													<p class="card-text">&#x20B1 ${data[i].price}</p>
													<h4 class="card-header"><a href="course.html?id=${data[i]._id}"class="card-header btn custom-button badge-pill mx-3">View Details</a></h4>
												</div>
												<div class="card-footer bg-dark">
													<a href="enroll.html?id=${data[i]._id}" class="btn text-light custom-button badge-pill"><i class="fas fa-plus mx-1" id="enroll"></i>Sign Up</a>
												</div>
											</div>
									 	 </div>`;
									}

									
									if(course){
										document.querySelector('#courseDetails').innerHTML = course;
									}

								}
								
								
								
							})

					}
				})

			
}else {
	const fetch = document.querySelector('#fetch');
	fetch.classList.remove('d-none')
}





